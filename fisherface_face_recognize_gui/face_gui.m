function varargout = face_gui(varargin)
% FACE_GUI MATLAB code for face_gui.fig
%      FACE_GUI, by itself, creates a new FACE_GUI or raises the existing
%      singleton*.
%
%      H = FACE_GUI returns the handle to a new FACE_GUI or the handle to
%      the existing singleton*.
%
%      FACE_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FACE_GUI.M with the given input arguments.
%
%      FACE_GUI('Property','Value',...) creates a new FACE_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before face_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to face_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help face_gui

% Last Modified by GUIDE v2.5 15-Feb-2017 22:29:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @face_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @face_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before face_gui is made visible.
function face_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to face_gui (see VARARGIN)

% Choose default command line output for face_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes face_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = face_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global im;
[filename, pathname] = uigetfile({'*.bmp'},'选择图片');
str = [pathname, filename];
im = imread(str);
axes( handles.axes1);
imshow(im);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global im
global base
global newsample
global address
global sw
global sb

% 预处理新数据
im = double(im(:)');
aimone = 1;

vsort1=projectto(sw,sb,24);
%训练样本和测试样本分别投影
tstsample=im*base*vsort1;
trainsample=newsample*vsort1;

for j=1:200
    juli(j)=norm(tstsample-trainsample(j,:));
end
[temp index]=sort(juli);
aimone = ceil(index(1)/5)

listnum = unidrnd(5)
aimpath=strcat(address,num2str(aimone),'_',num2str(listnum),'.bmp');
axes( handles.axes2 )
imshow(aimpath)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global address
global sample_class
global test
global sw
global sb
global base
global newsample
global test_num
global train_num
global train_samplesize

sample_class=1:40;%样本类别
sample_classnum=size(sample_class,2);%样本类别数
fprintf('训练开始....................\n\n');
set(handles.edit1,'string','训练开始');

train_samplesize=5;
train=1:train_samplesize;%每类训练样本
test=train_samplesize+1:10;%每类测试样本

train_num=size(train,2);%每类训练样本数
test_num=size(test,2);%每类测试样本数

address=[pwd '\ORL\s'];
%读取训练样本
allsamples=readsample(address,sample_class,train);

%先使用PCA进行降维
[newsample base]=pca(allsamples,0.9);
%计算Sw,Sb
[sw sb]=computswb(newsample,sample_classnum,train_num);

fprintf('训练结束....................\n\n');
set(handles.edit1,'string','训练结束');

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global address
global sample_class
global test
global sw
global sb
global base
global newsample
global test_num
global train_num
global train_samplesize

%读取测试样本
testsample=readsample(address,sample_class,test);
best_acc=0;%最优识别率
%寻找最佳投影维数
for temp_dimension=1:1:length(sw)
    vsort1=projectto(sw,sb,temp_dimension);

    %训练样本和测试样本分别投影
    tstsample=testsample*base*vsort1;
    trainsample=newsample*vsort1;
    %计算识别率
    accuracy=computaccu(tstsample,test_num,trainsample,train_num);
    if accuracy>best_acc
        best_dimension=temp_dimension;%保存最佳投影维数
        best_acc=accuracy;
    end
end
%---------------------------------输出显示----------------------------------
fprintf('每类训练样本数为：%d\n',train_samplesize);
fprintf('最佳投影维数为：%d\n',best_dimension);
fprintf('FisherFace的识别率为：%.2f%%\n',best_acc*100);
set(handles.edit1,'string',best_acc*100);
