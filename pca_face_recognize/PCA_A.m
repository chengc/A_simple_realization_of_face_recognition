clc
clear all
close all


R=[]; %样本矩阵
for i=1:40
    for j=1:3
        a=imread(strcat('ORL\s',num2str(i),'\',num2str(j),'.pgm'));%读入训练图片
        b=double(a(1:10304)');%矩阵向量化
        R(:,(i-1)*3+j)=b(:,1);%R是10304×200的矩阵  
    end
end
Rmean=mean(R,2);%样本均值、是一个10304*1矩阵
for i=1:120
    e(:,i)=R(:,i)-Rmean();%每张人脸与平均脸的差值，是一个10304×200矩阵
end
C=e'*e;%协方差矩阵C'=e*e',维数和计算量较大，通过求e'*e的特征值和特征向量获得的e*e'的特征值和特征向量
[V, D]=eig(C);%V，D分别是C的特征值和特征向量
D1=diag(D);
[D_sort, D_index]= sort(D1,'descend');% 降序排序，D_sort是排序后的特征值，D_index是排序的原序号
V_sort=V(:, D_index);% V_sort就是对应排序后的特征向量
p=0;
Dsum=0;
while(Dsum/sum(D_sort)<0.9)%选取前p个占95%能量的特征值
    p=p+1;
    Dsum=sum(D_sort(1:p));
end
i=1;
while (i<=p && D_sort(i)>0)
    w(:,i) =  e * V_sort(:,i)/D_sort(i)^(1/2);   % w是10304×p阶矩阵,是原协方差矩阵的特征向量
    i = i + 1;
end
w1=w/(w'*w);
F=w1'*R;%将所有样本投影到特征空间，F是p*200阶矩阵



R2=[]; %样本矩阵
for i=1:40
    for j=4:10
        a2=imread(strcat('ORL\s',num2str(i),'\',num2str(j),'.pgm'));%读入训练图片
        b2=double(a2(1:10304)');%矩阵向量化
        R2(:,(i-1)*7+j-3)=b2(:,1);%R是10304×200的矩阵  
    end
end

F2=w1'*R2;%将所有样本投影到特征空间，F是p*200阶矩阵

accu=0;
testsampnum=size(F2,2);
trainsampnum=size(F,2);
for i=1:testsampnum
    for j=1:trainsampnum
        juli(j)=norm(F2(:,i)-F(:,j));
    end
    [temp index]=sort(juli);
    if ceil(i/7)==ceil(index(1)/3)
        accu=accu+1;
    end
end
accuracy=accu/testsampnum;
fprintf('A组：每类训练样本数为 3 ，每类测试样本数为 7 \n');
fprintf('识别率为：%.2f%%\n',accuracy*100);